package com.visby.visby_app.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.visby.visby_app.R;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        //loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);

        //if (loginPreferences.getBoolean("saveLogin", false)) {
        ImageView imageView = findViewById(R.id.IvGif);

        Glide
                .with(this)
                .load(R.drawable.ecg_gif)
                .into(imageView);
            Thread myThread = new Thread() {

                @Override
                public void run() {
                    try {
                        sleep(4000);
                        Log.i("==Splash1", "Splash111");

                        Intent i = new Intent(getApplicationContext(), SignInActivity.class);
                        startActivity(i);
                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.i("==SplashErr", e.getMessage().toString());

                    }
                }

            };
            myThread.start();
//        } else {
//            Thread myThread = new Thread() {
//
//                @Override
//                public void run() {
//                    try {
//                        sleep(2000);
//                        Log.i("==Splash1", "Splash222");
//
//                        Intent i = new Intent(getApplicationContext(), WelcomeActivity.class);
//                        startActivity(i);
//                        finish();
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                        Log.i("==SplashErr111", e.getMessage().toString());
//
//                    }
//                }
//
//            };
//            myThread.start();
//        }
    }
}