package com.visby.visby_app.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.exifinterface.media.ExifInterface;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.system.ErrnoException;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImageView;
import com.visby.visby_app.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static android.Manifest.permission.ACCESS_BACKGROUND_LOCATION;
import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage;

public class CaptureResultActivity extends AppCompatActivity {

    private CardView mBtnCustom;

    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 88;
    private Uri mCropImageUri;
    CropImageView mCropImageView;
    private RelativeLayout releCrop;
    private LinearLayout llMain;
    private int selection;
    File filePath = null;
    Button CropImageViewNo, CropImageView1, CropRotate;
    String profileImageBase64;
    String mediaPath;
    private ImageView Iv_Camera;
    private ImageView backBtn;
    private ImageView mHomeBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_result);
        if (ActivityCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(CaptureResultActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

        }

        checkPermission();
        initViews();
    }


    private void initViews() {
        mHomeBtn = findViewById(R.id.home_btn);
        ImageView BtnBack = findViewById(R.id.BtnBack);
        TextView mTvActionbarTitle = findViewById(R.id.TvActionbarTitle);
        TextView mBtnTitle = findViewById(R.id.tvBtnTitle);
//        mBtnCustom = findViewById(R.id.BtnCustom);
        mTvActionbarTitle.setText(getResources().getString(R.string.capture_result));
        mBtnTitle.setText(getResources().getString(R.string.submit));
        //TextView tvCaptureResult = findViewById(R.id.tvCaptureResult);
        soup.neumorphism.NeumorphCardView BtnCapture = findViewById(R.id.BtnCapture);
        BtnCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (filePath != null) {
                    Intent intent = new Intent(CaptureResultActivity.this, TestResultActivity.class);
                    intent.putExtra("image", filePath.getPath());
                    Log.i("image111", String.valueOf(filePath));
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(CaptureResultActivity.this, TestResultActivity.class);

                    startActivity(intent);
                }
            }
        });
//        tvCaptureResult.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
        BtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//        mBtnCustom.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (filePath != null) {
//                    Intent intent = new Intent(CaptureResultActivity.this, TestResultActivity.class);
//                    intent.putExtra("image", filePath.getPath());
//                    Log.i("image111", String.valueOf(filePath));
//                    startActivity(intent);
//                }else{
//                    Intent intent = new Intent(CaptureResultActivity.this, TestResultActivity.class);
//
//                    startActivity(intent);
//                }
//            }
//        });


        // capture image

        Iv_Camera = (ImageView) findViewById(R.id.Iv_Camera);
        Iv_Camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selection = 1;
                startActivityForResult(getPickImageChooserIntent(), 200);
            }
        });
        releCrop = (RelativeLayout) findViewById(R.id.releCrop);
        llMain = (LinearLayout) findViewById(R.id.llMain);
        CropImageViewNo = (Button) findViewById(R.id.CropImageViewNo);
        CropImageView1 = (Button) findViewById(R.id.CropImageView1);
        //  CropRotate = (Button) findViewById(R.id.CropRotate);
        mCropImageView = (CropImageView) findViewById(R.id.CropImageView);

        CropImageViewNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                releCrop.setVisibility(View.GONE);
                llMain.setVisibility(View.VISIBLE);

            }
        });


        CropImageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bitmap cropped = mCropImageView.getCroppedImage();
                Log.i("=dwdss", String.valueOf(cropped));
                if (cropped != null)
                    releCrop.setVisibility(View.GONE);
                llMain.setVisibility(View.VISIBLE);
                convertImageToBase64(cropped);
                //  Iv_profile.setImageBitmap(cropped);


                try {
                    saveIntoFile(cropped);

                } catch (IOException e) {
                    e.printStackTrace();
                    Log.i("==errSaveFil", e.getMessage());
                }


//                Picasso.with(AthleteProfileUpdateActivity.this)
//                        .load(filePath) // web image url
//                        .fit().centerInside()
//                        .rotate(90)                    //if you want to rotate by 90 degrees
//                        .into(Iv_profile);
            }
        });

        mHomeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CaptureResultActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });
    }

    // permission check
    private void checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED

                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED

                && ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(CaptureResultActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1);
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(CaptureResultActivity.this, "Permission denied !", Toast.LENGTH_SHORT).show();
                }
                return;
            }


            // other 'case' lines to check for other
            // permissions this app might request

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Toast.makeText(this, "" + requestCode + " -- Res code : " + resultCode, Toast.LENGTH_SHORT).show();

        if (resultCode == Activity.RESULT_OK) {
            Uri imageUri = getPickImageResultUri(data);
            releCrop.setVisibility(View.VISIBLE);
            llMain.setVisibility(View.GONE);
            boolean requirePermissions = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    isUriRequiresPermissions(imageUri)) {
                requirePermissions = true;
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_CAMERA);
            }

            if (!requirePermissions) {
                //mCropImageView.setImageUriAsync(imageUri);
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(CaptureResultActivity.this.getResources(), bitmap);
                    circularBitmapDrawable.setCircular(true);
                    // mCropImageView.setImageBitmap(circularBitmapDrawable.getBitmap());

                    // mCropImageView.rotateImage(90);
                    Bitmap bitmap1 = handleSamplingAndRotationBitmap(getApplicationContext(), imageUri);
                    mCropImageView.setImageBitmap(bitmap1);
                    Iv_Camera.setImageBitmap(bitmap1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (resultCode == 0) {
            releCrop.setVisibility(View.GONE);
            llMain.setVisibility(View.VISIBLE);
        }

    }

//    public void BtnSubmit(View view) {
//        if (filePath != null) {
//                    Intent intent = new Intent(CaptureResultActivity.this, TestResultActivity.class);
//                    intent.putExtra("image", filePath.getPath());
//                    Log.i("image111", String.valueOf(filePath));
//                    startActivity(intent);
//                }else{
//                    Intent intent = new Intent(CaptureResultActivity.this, TestResultActivity.class);
//
//                    startActivity(intent);
//                }
//    }

    /*
     * Create a chooser intent to select the  source to get image from.
     * The source can be camera's  (ACTION_IMAGE_CAPTURE) or gallery's
     * (ACTION_GET_CONTENT).
     * All possible sources are added to the intent chooser.
     */
    public Intent getPickImageChooserIntent() {
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /*
        Get URI to image received from capture  by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    /*
     * Get the URI of the selected image from
     * getPickImageChooserIntent().
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the  activity result
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }


    /**
     * Test if we can open the given Android URI to test if permission
     * required error is thrown.
     * Only relevant for API version 23 and above.
     *
     * @param uri the result URI of image pick.
     */
    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (e.getCause() instanceof ErrnoException) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    /*
       Temporary storage of user selected profile picture in
       user's device. This file's path will be used to send image file
       to server.

       @param bitmap selected by user.
    */
    public void saveIntoFile(Bitmap bitmap) throws IOException {

        Random random = new Random();
        int ii = 100000;
        ii = random.nextInt(ii);
        String fname = "000_" + ii + ".jpg";
//        String fname = "MyPic1.jpg";
        File direct = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + fname);

        if (!direct.exists()) {
            File wallpaperDirectory = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/");
            wallpaperDirectory.mkdirs();
        }

        File mainfile = new File(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/"), fname);
        if (mainfile.exists()) {
            mainfile.delete();
        }

        FileOutputStream fileOutputStream;
        fileOutputStream = new FileOutputStream(mainfile);

        bitmap.compress(Bitmap.CompressFormat.JPEG, 20, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
        filePath = new File(mainfile.toString());
        Log.i("===FilePath", "File path is " + filePath.toString());

    }

    private void convertImageToBase64(Bitmap bm) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos); //bm is the bitmap object
        byte[] byteArrayImage = baos.toByteArray();
        profileImageBase64 = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
        //Log.i("==Base64",profileImageBase64);

    }

    /**
     * This method is responsible for solving the rotation issue if exist. Also scale the images to
     * 1024x1024 resolution
     *
     * @param context       The current context
     * @param selectedImage The Image URI
     * @return Bitmap image results
     * @throws IOException
     */
    public static Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage)
            throws IOException {
        int MAX_HEIGHT = 1024;
        int MAX_WIDTH = 1024;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
        BitmapFactory.decodeStream(imageStream, null, options);
        imageStream.close();

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        imageStream = context.getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

        img = rotateImageIfRequired(context, img, selectedImage);
        return img;
    }

    /**
     * Calculate an inSampleSize for use in a {@link BitmapFactory.Options} object when decoding
     * bitmaps using the decode* methods from {@link BitmapFactory}. This implementation calculates
     * the closest inSampleSize that will result in the final decoded bitmap having a width and
     * height equal to or larger than the requested width and height. This implementation does not
     * ensure a power of 2 is returned for inSampleSize which can be faster when decoding but
     * results in a larger bitmap which isn't as useful for caching purposes.
     *
     * @param options   An options object with out* params already populated (run through a decode*
     *                  method with inJustDecodeBounds==true
     * @param reqWidth  The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @return The value to be used for inSampleSize
     */
    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            final float totalPixels = width * height;

            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    /**
     * Rotate an image if required.
     *
     * @param img           The image bitmap
     * @param selectedImage Image URI
     * @return The resulted Bitmap after manipulation
     */
    private static Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage) throws IOException {

        InputStream input = context.getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else
            ei = new ExifInterface(selectedImage.getPath());

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }


}